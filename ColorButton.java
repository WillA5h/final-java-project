import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import java.awt.Color;

public class ColorButton extends JButton implements ActionListener{

	private int buttonNum;
	private Driver driveObj;

	private int state;
	//0 = no flip
	//1 = flip
	//2 = done
	
	private Color clr;
	private int check;
	private boolean enable;

	public ColorButton(int butNum, Driver tempDriver, int minH, int  minW)
	{
		buttonNum = butNum;
		driveObj = tempDriver;
		enable = true;
		Dimension dim = new Dimension(minW, minH);
		setMinimumSize(dim);
		setPreferredSize(dim);
		addActionListener(this);
	}

	public void flip(boolean flipped)
	{
		if (state == 2)
		{
			return;
		}
		else 
		{
			if(flipped == true)
			{
				state = 1;
			}
			else
			{
				state = 0;
			}
		}
		
	}
	
	public void setEnable(boolean en)
	{
		enable = en;
	}
	
	public boolean getEnable()
	{
		return enable;
	}
	
	public void setCheck(int num)
	{
		check = num;
	}
	
	public int getCheck()
	{
		return check;
	}
	
	public void setColor(Color clr)
	{
		this.clr = clr;
	}
	
	public void setState(int st)
	{
		state = st;
		repaint();
	}
	
	public int getState()
	{
		return state;
	}
	
	public void paintComponent(Graphics g)
	{
		if (state == 0)
		{
			g.setColor(Color.darkGray);
			g.fillRect(0, 0, getWidth(), getHeight());
		}
		else if (state == 1)
		{
			g.setColor(clr);
			g.fillRect( 5, 5,	getWidth() - 10, getHeight() - 10 );
		}
		else if (state == 2)
		{
			g.setColor(Color.black);
			g.fillRect(0, 0, getWidth(), getHeight());
		}
	}

	@Override
	public void actionPerformed(ActionEvent arg0) {
		driveObj.buttonClicked(buttonNum);	
	}
	
}
