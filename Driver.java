import javax.swing.*;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridLayout;
import java.util.*;

public class Driver {
	final static int WIDTH = 2;
	final static int HEIGHT = 2;
	final static int COLOURS = 2;
	final static int MAX_PER_COLOUR = 2;
	final static int BUT_SIZE = 150;

	private JFrame jf = new JFrame();
	private JLabel jl1 = new JLabel();
	private JLabel jl2 = new JLabel();
	private JLabel jlgrid = new JLabel();
	private Font font = new Font("times", Font.PLAIN, 12);
	private ColorButton[] buttons = new ColorButton[WIDTH * HEIGHT];
	private Color[] colors = new Color[WIDTH * HEIGHT];
	private Random rand = new Random();
	private Dimension dim = new Dimension(BUT_SIZE*WIDTH, BUT_SIZE*HEIGHT);

	private int[] scores = new int[2];
	private int turn = 0;
	private int stage = 0;
	private boolean success = true;
	private int firstid;
	private int secid;
	private int count;


	Driver()
	{
		jf.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		jf.setLayout(new BorderLayout());
		jf.setTitle("Colour Matching Game");

		jl1.setOpaque(true);
		jl1.setForeground(Color.black);
		jl1.setFont(font);

		jl2.setOpaque(true);
		jl2.setForeground(Color.black);
		jl2.setFont(font);

		jf.add(jl1, BorderLayout.NORTH);
		

		
		jlgrid.setLayout(new GridLayout(WIDTH, HEIGHT));
		for (int i=0; i<WIDTH*HEIGHT; i++)
		{
			ColorButton button = new ColorButton(i, this, BUT_SIZE, BUT_SIZE);
			jlgrid.add(button);
			buttons[i] = button;
		}
		jf.add(jlgrid, BorderLayout.CENTER);
		
		jf.add(jl2, BorderLayout.SOUTH);
		
		setup();

		jf.setMinimumSize(dim);
		jf.setVisible(true);
	}

	void buttonClicked(int iButton)
	{
		if (buttons[iButton].getEnable() == true)
		{
			if (firstid == -1)
			{
				firstid = iButton;
				ColorButton temp = buttons[firstid];
				temp.setState(1);
				stage = 1;
				temp.setEnable(false);
				changeMessage();
			}
			else if (firstid != -1 && secid == -1)
			{
				secid = iButton;
				ColorButton temp1 = buttons[firstid];
				ColorButton temp2 = buttons[secid];
				temp2.setState(1);
				temp2.setEnable(false);
				stage = 2;
				if (temp1.getCheck() == temp2.getCheck())
				{
					success = true;
					scores[turn] = scores[turn] + 2;
					changeMessage();
					temp1.setEnable(true);
					temp2.setEnable(true);
				}
				else
				{
					success = false;
					changeMessage();
					temp1.setEnable(true);
					temp2.setEnable(true);
				}
			}
			else if (firstid != -1 && secid != -1)
			{
				stage = 0;
				if (success == true)
				{
					ColorButton temp1 = buttons[firstid];
					ColorButton temp2 = buttons[secid];
					temp1.setState(2);
					temp2.setState(2);

					temp1.setEnable(false);
					temp2.setEnable(false);
					count++;

				}
				if (success == false)
				{
					if (turn == 1) turn = 0;
					else if (turn == 0) turn = 1;
					ColorButton temp1 = buttons[firstid];
					ColorButton temp2 = buttons[secid];
					temp1.setState(0);
					temp2.setState(0);
				}
				success = false;
				firstid = -1;
				secid = -1;
				changeMessage();
				if (count == COLOURS)
				{
					endGame();
					setup();
				}
			}
		}
	}

	public void endGame()
	{
		if (scores[0] == scores[1])
		{
			JOptionPane.showMessageDialog(null, "Its a draw!");
		}
		else if (scores[0] > scores[1])
		{
			JOptionPane.showMessageDialog(null, "Player 0 wins!");
		}
		else
		{
			JOptionPane.showMessageDialog(null, "Player 1 wins!");
		}

	}

	public void setup()
	{
		turn = 0;
		stage = 0;
		firstid = -1;
		secid = -1;
		success = false;
		scores[0] = 0;
		scores[1] = 0;
		count = 0;
		Arrays.fill(colors, null);

		for (int i=1; i<=WIDTH*HEIGHT; i=i+MAX_PER_COLOUR)
		{
			float r = rand.nextFloat();
			float g = rand.nextFloat();
			float b = rand.nextFloat();
			Color temp = new Color(r,g,b);
			for (int j = 0; j<2; j++)
			{
				boolean done = false;
				while (done == false)
				{
					int randomInt = rand.nextInt(buttons.length);
					if (colors[randomInt] == null)
					{
						colors[randomInt] = temp;
						done = true;
						buttons[randomInt].setCheck(i);
					}
				}
			}
		}

		for (int i = 0; i<buttons.length; i++)
		{
			buttons[i].setColor(colors[i]);
			buttons[i].setState(0);
			buttons[i].setEnable(true);
		}

		changeMessage();		
	}

	public void changeMessage() {
		if (turn == 0)
		{
			jl2.setText("Player 1, score : "+scores[1]+" - Player 0's turn, Please wait.");
			jl2.setBackground(Color.red);
			jl1.setBackground(Color.green);
			if (stage == 0)
			{
				jl1.setText("*** Player 0, score : "+scores[0]+" - Choose your first square");
			}
			else if (stage == 1)
			{
				jl1.setText("*** Player 0, score : "+scores[0]+" - Choose your second square");
			}
			else if (stage == 2 && success == true)
			{
				jl1.setText("*** Player 0, score : "+scores[0]+" - Well done, click a square to continue...");
			}
			else if (stage == 2 && success == false)
			{
				jl1.setText("*** Player 0, score : "+scores[0]+" - Failed, click a square to end turn...");
			}
		}
		else if (turn == 1)
		{
			jl1.setText("Player 0, score : "+scores[0]+" - Player 1's turn, Please wait.");
			jl1.setBackground(Color.red);
			jl2.setBackground(Color.green);
			if (stage == 0)
			{
				jl2.setText("*** Player 1, score : "+scores[1]+" - Choose your first square");
			}
			else if (stage == 1)
			{
				jl2.setText("*** Player 1, score : "+scores[1]+" - Choose your second square");
			}
			else if (stage == 2 && success == true)
			{
				jl2.setText("*** Player 1, score : "+scores[1]+" - Well done, click a square to continue...");
			}
			else if (stage == 2 && success == false)
			{
				jl2.setText("*** Player 1, score : "+scores[1]+" - Failed, click a square to end turn...");
			}
		}
	}

	public static void main(String[] args) {
		new Driver();
	}
}
